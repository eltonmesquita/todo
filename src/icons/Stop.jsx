import React from 'react'
import PropTypes from 'prop-types'

const Stop = ({ size = 24, fill }) => (
  <svg
    width={size}
    height={size}
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 512 512"
  >
    <path fill={fill} d="M452 0H60C26.916 0 0 26.916 0 60v392c0 33.084 26.916 60 60 60h392c33.084 0 60-26.916 60-60V60c0-33.084-26.916-60-60-60zm20 452c0 11.028-8.972 20-20 20H60c-11.028 0-20-8.972-20-20V60c0-11.028 8.972-20 20-20h392c11.028 0 20 8.972 20 20v392z" />
  </svg>
)

Stop.propTypes = {
  size: PropTypes.number,
  fill: PropTypes.resetWarningCache
}

export default Stop