import Trash from './Trash';
import Play from './Play';
import Record from './Record';
import Stop from './Stop';
export {
  Trash,
  Play,
  Record,
  Stop,
}