import React from 'react'
import { render } from '@testing-library/react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'

import reducers from '../reducers'
import { recordingMiddleware } from '../utilities'

const store = createStore(reducers, applyMiddleware(recordingMiddleware))

/* eslint-disable */
const AllTheProviders = ({ children }) => {
  return <Provider store={store}>{children}</Provider>
}
/* eslint-enable */

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options })

// re-export everything
export * from '@testing-library/react'

// override render method
export { customRender as render }
