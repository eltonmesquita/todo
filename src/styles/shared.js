export default {
  colours: {
    brand: '#058ed8',
    highlight: '#f7ab1b',
    background: '#f5f5f5',
    border: '#ccc',
    dark: '#333',
    success: '#0cb949',
    attention: '#f5006b'
  },
}
