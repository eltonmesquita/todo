const loadState = () => {
  try {
    const local = localStorage.getItem('state')
    if (local === null) return undefined

    return JSON.parse(local)
  } catch (err) {
    return undefined
  }
}

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch (err) {
    console.warn('There was an error writting to localStorage.')
  }
}

export { loadState, saveState }
