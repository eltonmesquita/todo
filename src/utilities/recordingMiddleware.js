import { ADD_RECORDING } from '../actions'

const recordingMiddleware = (store) => (next) => (action) => {
  let result
  result = next(action)
  
  const isRecordingAction = action.type.includes('RECORDING')
  const state = store.getState()

  if (!isRecordingAction && state.recording.isRecording) {
    result = next({ type: ADD_RECORDING, state: state.todos })
  }

  return result
}

export default recordingMiddleware
