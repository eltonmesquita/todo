import React from 'react'
import {
  render,
  fireEvent,
  cleanup,
} from './test-utils'

import App from './App'
import { generateId } from './utilities'

jest.mock('./utilities/generateId.js')

beforeEach(cleanup)

describe('When application runs', () => {
  it('runs without crashing', async () => {
    const wrapper = render(<App />)

    wrapper.getByText('ToDo App')
    wrapper.getByTestId('addTodo--input')
  })
})

describe('When using tasks', () => {
  it('adds new task', async () => {
    generateId.mockImplementation(() => 'key-01')
    const wrapper = render(<App />)
    const input = wrapper.getByTestId('addTodo--input')

    fireEvent.click(input)
    fireEvent.change(input, { target: { value: 'Pass this test' } })
    fireEvent.click(wrapper.getByText('Add'))

    await wrapper.findByText('Pass this test')
  })

  it('marks a task as done and not done', async () => {
    const wrapper = render(<App />)

    fireEvent.click(wrapper.getByTestId('checkbox_key-01'))
    wrapper.findByText('✓')

    fireEvent.click(wrapper.getByTestId('checkbox_key-01'))
    expect(wrapper.queryByText('✓')).toBeNull()
  })
  
  it('deletes a task', async () => {
    const wrapper = render(<App />)
    
    fireEvent.click(wrapper.getByTestId('remove_key-01'))
    expect(wrapper.queryByTestId('task_key-01')).toBeNull()
  })
})

describe('When recording', () => {
  it('records successfully', async () => {
    generateId.mockImplementation(() => 'key-01')
    const wrapper = render(<App />)
    const input = wrapper.getByTestId('addTodo--input')

    fireEvent.click(wrapper.getByTestId('recording_record'))
    fireEvent.change(input, { target: { value: 'Pass this test' } })
    fireEvent.click(wrapper.getByText('Add'))
    
    generateId.mockImplementation(() => 'key-02')
    fireEvent.change(input, { target: { value: 'Do some stuff' } })
    fireEvent.click(wrapper.getByText('Add'))
    
    await wrapper.findByText('Pass this test')
    await wrapper.findByText('Do some stuff')
    fireEvent.click(wrapper.getByTestId('checkbox_key-02'))

    fireEvent.click(wrapper.getByTestId('recording_record'))
  })
})
