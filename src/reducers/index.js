import { combineReducers } from 'redux'
import todos from './todos'
import recording from './recording'

const reducers = combineReducers({
  todos,
  recording,
})

export default reducers
