import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO, CLEAR_TODOS, REPLACE_TODOS } from '../actions'

import { generateId } from '../utilities'

function addTodo(state, action) {
  const newTodo = {
    id: generateId(),
    done: false,
    title: action.title,
    description: action.description,
    createdAt: action.createdAt
  }

  return [...state, newTodo]
}

function removeTodo(state, action) {
  const todos = state.filter((todo) => todo.id !== action.id)

  return todos
}

function toggleTodo(state, action) {
  const todos = state.map((todo) =>
    todo.id === action.id ? { ...todo, done: !todo.done } : todo
  )

  return todos
}

function todos(state = [], action) {
  switch (action.type) {
    case ADD_TODO:
      return addTodo(state, action)
    case REPLACE_TODOS:
      return action.todo
    case REMOVE_TODO:
      return removeTodo(state, action)
    case TOGGLE_TODO:
      return toggleTodo(state, action)
    case CLEAR_TODOS:
      return []
    default:
      return state
  }
}

export default todos
