import {
  START_RECORDING,
  STOP_RECORDING,
  PLAY_RECORDING,
  CLEAR_RECORDING,
  ADD_RECORDING,
  STOP_PLAYING_RECORD,
} from '../actions'

const inititalState = {
  isRecording: false,
  recordings: [],
  isPlaying: false,
}

function recording(state = inititalState, action) {
  switch (action.type) {
    case START_RECORDING:
      return { ...state, recordings: [], isRecording: true }
    case ADD_RECORDING:
      return { ...state, recordings: [...state.recordings, action.state] }
    case STOP_RECORDING:
      return { ...state, isRecording: false }
    case PLAY_RECORDING:
      return { ...state, isPlaying: true }
    case STOP_PLAYING_RECORD:
      return { ...state, isPlaying: false }
    case CLEAR_RECORDING:
      return { ...state, recordings: [] }
    default:
      return state
  }
}

export default recording
