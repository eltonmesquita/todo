import React from 'react'

import { Globals } from './styles/'
import Container from './components/Container'
import List from './components/List'
import AddTodo from './components/AddTodo'
import RecordingBar from './components/RecordingBar'
import Header from './components/Header'

const App = () => (
  <main>
    <Header />
    <Container>
      <Globals />
      <AddTodo />
      <List />
      <RecordingBar />
    </Container>
  </main>
)

export default App
