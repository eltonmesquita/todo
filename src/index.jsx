import React from 'react'
import { render } from 'react-dom'
import { createStore, compose, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import throttle from 'lodash/throttle'

import reducers from './reducers'
import { saveState, loadState, recordingMiddleware } from './utilities'

import App from './App'

const persistedData = loadState()
const devTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const store = createStore(
  reducers,
  persistedData,
  compose(applyMiddleware(recordingMiddleware), devTools)
)

store.subscribe(
  throttle(() => {
    const state = store.getState()
    const recording = {
      isRecording: false,
      recordings: state.recording.recordings,
      isPlaying: false,
    }

    saveState({ ...state, recording })
  }, 1200)
)

const container = document.getElementById('root')

render(
  <Provider store={store}>
    <App />
  </Provider>,
  container
)
