import React from 'react'
import styled from '@emotion/styled'
import { shared } from '../../styles'

const Wrap = styled.header`
  padding: 1rem;
  padding-bottom: 8rem;

  background: linear-gradient(145deg, #016ba5 0%, ${shared.colours.brand} 100%);
`

const Title = styled.h1`
  margin-top: 0;

  color: #fff;
  text-align: center;
`

const Header = () => (
  <Wrap>
    <Title>ToDo App</Title>
  </Wrap>
)

export default Header
