import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from '@emotion/styled'

import Item from '../Item'
import { TOGGLE_TODO, REMOVE_TODO } from '../../actions'

const Todos = styled.ul`
  margin: 0;
  padding: 0;
`

const List = () => {
  const dispatch = useDispatch()
  const todos = useSelector((state) => state.todos)
  const toggleItem = (id) => dispatch({ type: TOGGLE_TODO, id })
  const removeItem = (id) => dispatch({ type: REMOVE_TODO, id })

  return (
    <Todos>
      {todos.map((todo) => (
        <Item
          id={todo.id}
          done={todo.done}
          remove={removeItem}
          toggle={toggleItem}
          key={todo.id}
          title={todo.title}
          createdAt={todo.createdAt}
        >
          {todo.description}
        </Item>
      ))}
    </Todos>
  )
}
export default List
