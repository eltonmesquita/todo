import React from 'react'
import PropTypes from 'prop-types'

import { Trash } from '../../icons'
import styled from '@emotion/styled'
import { shared } from '../../styles'

const Task = styled.li`
  display: flex;
  padding: 1rem;
  margin-bottom: 1.5rem;

  border-left: 0.5rem solid
    ${(props) => (props.done ? shared.colours.success : shared.colours.brand)};
  border-radius: 2px 8px 8px 2px;
  background: #fff;
  box-shadow: 0 0 18px rgba(0, 0, 0, 0.1);

  transition: 0.15s ease-out;

  &:hover {
    transform: scale(1.01);
  }
`

const CheckWrap = styled.label`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 4rem;

  cursor: pointer;
  text-align: center;
`

const Date = styled.div`
  margin-top: 1rem;

  color: #454545;
  font-size: 0.75rem;
  text-align: right;
`

const Title = styled.h3`
  margin: 0;
  margin-bottom: 0.5rem;
  padding-bottom: 0.5rem;

  color: ${(props) =>
    props.done ? shared.colours.success : shared.colours.dark};
  border-bottom: 1px solid ${shared.colours.border};

  text-decoration: ${(props) => (props.done ? 'line-through' : 'none')};
`

const Content = styled.div`
  width: calc(100% - 4rem);
`

const RemoveWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 4rem;
`

const Remove = styled.button`
  cursor: pointer;
  border: 0;
  box-shadow: none;
  background: transparent;
`

const Item = ({ id, done, remove, toggle, title, createdAt, children }) => (
  <Task done={done} data-testid={`task_${id}`}>
    <CheckWrap>
      <input
        data-testid={`checkbox_${id}`}
        type="checkbox"
        checked={done}
        onChange={() => toggle(id)}
      />
    </CheckWrap>
    <Content>
      <Title done={done}>
        {title}
        {done && ' ✓'}
      </Title>
      <div>
        {children}
        <Date>{createdAt}</Date>
      </div>
    </Content>
    <RemoveWrap>
      <Remove data-testid={`remove_${id}`} onClick={() => remove(id)}>
        <Trash size="24" fill={shared.colours.attention} />
      </Remove>
    </RemoveWrap>
  </Task>
)

Item.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  createdAt: PropTypes.string,
  done: PropTypes.bool,
  remove: PropTypes.func,
  toggle: PropTypes.func,
  children: PropTypes.node,
}

export default Item
