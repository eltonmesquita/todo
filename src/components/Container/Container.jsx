import styled from '@emotion/styled'

const Container = styled.div`
  max-width: 800px;
  width: 100%;
  margin: 0 auto;
  margin-bottom: 4rem;
  padding: 1rem;

  border-radius: 4px;
`

export default Container
