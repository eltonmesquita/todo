import React from 'react'
import styled from '@emotion/styled'

import { useDispatch } from 'react-redux'
import { ADD_TODO } from '../../actions'
import { shared } from '../../styles'

const Form = styled.form`
  margin: 0 auto;
  margin-top: -7rem;
  margin-bottom: 2rem;
  padding: 1rem;

  background: #fff;
  border-radius: 4px;
  box-shadow: 0 0 18px rgba(0, 0, 0, 0.1);

  @media (min-width: 500px) {
    display: flex;
  }
`

const Content = styled.div`
  @media (min-width: 500px) {
    width: 80%;
  }
`

const Input = styled.input`
  display: block;
  width: 100%;
  padding: 8px;
  margin-bottom: 1rem;

  border: 0;
  border-bottom: 1px solid ${shared.colours.border};
  border-radius: 0;
  font-size: 1.35rem;
`

const Textarea = styled.textarea`
  width: 100%;
  height: 5rem;
  padding: 8px;

  border: 0;
  border-bottom: 1px solid ${shared.colours.border};

  font-size: 1rem;
`

const ButtonWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 1rem;

  text-align: center;

  @media (min-width: 500px) {
    margin-top: 0;
    width: 20%;
  }
`

const Button = styled.button`
  padding: 0.5em 2em;

  color: #fff;
  background: ${shared.colours.highlight};
  border: 0;
  border-radius: 2px;

  font-size: 1rem;
`

const options = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  second: 'numeric',
  hour12: true,
}

const AddTodo = () => {
  let input
  let text
  const formatDate = (date) =>
    new Intl.DateTimeFormat('en-GB', options).format(date)

  const dispatch = useDispatch()
  const onSubmit = (e) => {
    e.preventDefault()
    if (!input.value.trim()) return false

    const createdAt = new Date()

    dispatch({
      type: ADD_TODO,
      title: input.value,
      description: text.value,
      createdAt: formatDate(createdAt),
    })
    input.value = ''
    text.value = ''
  }

  return (
    <Form onSubmit={onSubmit}>
      <Content>
        <Input
          data-testid="addTodo--input"
          type="text"
          placeholder="Create a task"
          ref={(node) => {
            input = node
          }}
        />
        <Textarea
          placeholder="Add a description to it"
          ref={(node) => {
            text = node
          }}
        />
      </Content>
      <ButtonWrap>
        <Button type="submit">Add</Button>
      </ButtonWrap>
    </Form>
  )
}

export default AddTodo
