import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  START_RECORDING,
  PLAY_RECORDING,
  CLEAR_RECORDING,
  STOP_RECORDING,
  CLEAR_TODOS,
  REPLACE_TODOS,
  STOP_PLAYING_RECORD,
} from '../../actions'
import styled from '@emotion/styled'

import { shared } from '../../styles'
import { Play, Record, Stop, Trash } from '../../icons'

const Wrap = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;

  width: 100%;
  padding: .75rem 1rem;

  background: ${shared.colours.brand};
  
  text-align: center;
`

const Button = styled.button`
  border: 0;
  background: transparent;

  cursor: pointer;
`

const RecordingBar = () => {
  const dispatch = useDispatch()

  const recording = useSelector((state) => state.recording)

  const record = () => {
    const action = recording.isRecording ? STOP_RECORDING : START_RECORDING
    dispatch({ type: action })
  }

  const nextStep = (todo, index, array) => {
    setTimeout(() => {
      dispatch({
        type: REPLACE_TODOS,
        todo,
      })

      index === array.length - 1 && dispatch({
        type: STOP_PLAYING_RECORD
      })
    }, 1000 * index)
  }

  const play = () => {
    if (recording.recordings.length > 0) {
      dispatch({ type: CLEAR_TODOS })
      dispatch({ type: PLAY_RECORDING })

      recording.recordings.forEach(nextStep)
    }
  }

  const clear = () => dispatch({ type: CLEAR_RECORDING })

  return (
    <Wrap>
      <Button data-testid="recording_record" onClick={record}>
        {recording.isRecording ? <Stop size={30} fill="#fff" /> : <Record size={30} fill="#fff" />}
      </Button>
      <Button data-testid="recording_play"onClick={play}><Play size={30} fill="#fff" /></Button>
      <Button data-testid="recording_clear" onClick={clear}><Trash  size={30} fill="#fff" /></Button>
    </Wrap>
  )
}

export default RecordingBar
