/* eslint-disable */
const debug = process.env.NODE_ENV !== 'production'
const webpack = require('webpack')
const path = require('path')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CircularDependencyPlugin = require('circular-dependency-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
  mode: process.env.NODE_ENV,
  entry: './src/index.jsx',

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  output: {
    publicPath: '/',
    path: path.join(__dirname, 'build'),
    filename: 'js/[name].bundle.min.js',
    chunkFilename: 'js/[name].bundle.js',
  },

  devtool: debug ? 'cheap-module-eval-source-map' : false,

  devServer: {
    inline: true,
    contentBase: './src',
    port: 3000,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
          presets: ['@babel/env', '@babel/preset-react'],
        },
      },
      {
        test: /\.(gif|png|jpe?g)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/images/',
            },
          },
        ],
      },
    ],
  },

  plugins: debug
    ? [
        new CircularDependencyPlugin({
          exclude: /a\.js|node_modules/,
          failOnError: true,
          cwd: process.cwd(),
        }),
        new HtmlWebpackPlugin({
          template: './src/index.html',
        }),
      ]
    : [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('production'),
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.LoaderOptionsPlugin({
          minimize: true,
        }),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new HtmlWebpackPlugin({
          template: './src/index.html',
        }),
        new CompressionPlugin({
          test: /\.(html|css|js|gif|svg|ico|woff|ttf|eot)$/,
          exclude: /(node_modules)/,
        })
      ],

  optimization: {
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          ie8: true,
          safari10: true,
          sourceMap: true,
        },
      }),
    ],
  },
}
