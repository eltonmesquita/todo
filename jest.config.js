// eslint-disable-next-line
module.exports = {
  clearMocks: true,
  moduleDirectories: [
    'node_modules',
    'src/text-utils'
  ],
  testEnvironment: 'jsdom',
};
